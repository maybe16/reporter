'use strict';
var config = require('./webpack.config.js');

var http = require('http'),
	u = require('./server/utils'),
	l = require('./server/logger')(module),
	projectManager = require('./server/project_manager'),
	router = require('./server/router'),
	fs = require('fs'),
	iconv = require('iconv-lite'),
	apiRoutes;


apiRoutes = {
	projects: {
		create: function(data, req, res){
			projectManager.create(data.origName, res);
		},
		remove: function(data, req, res){
			projectManager.remove(data.id, res);
		},
		get: function(data, req, res){
			projectManager.get(data.id, res);
		},
		getAllInfo: function(data, req, res){
			projectManager.getAllInfo(res);
		},
		update: function(data, req, res){
			projectManager.update(data, res);
		},
		parseProjectSource: function(data, req, res){
			projectManager.parseProjectSource(data.id, res);
		},
	}
};
//
router.setAPIConfig(apiRoutes);

http.createServer(function (req, res) {
	res.setHeader("Access-Control-Allow-Origin", "*"); // IE9 needs this.
	router.process(req, res);
}).listen(config.API_SERVER_PORT, config.HOST, function() {
  console.log(`Started on ${config.API_SERVER_URL}`);
});
