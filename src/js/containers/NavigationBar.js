import React, { Component } from 'react';
import { connect } from 'react-redux';

import { openModal } from '../actions/Modals';
import { PROJECT_MANAGER_MODAL_NAME } from '../constants/ModalsNames';
import { Navbar, NavItem, Nav, NavDropdown, MenuItem } from 'react-bootstrap';

class NavigationBar extends Component {
  render() {
    const { openProjectManager, currentProject } = this.props;
    const projectName = currentProject.origName || 'Select project';
    return (
      <Navbar>
        <Navbar.Header>
          <Navbar.Brand>
            <a href="#" onClick={openProjectManager}>{projectName}</a>
          </Navbar.Brand>
        </Navbar.Header>
        <Nav>
          <NavItem eventKey={1} href="#">Link</NavItem>
          <NavItem eventKey={2} href="#">Link</NavItem>
          <NavDropdown eventKey={3} title="Dropdown" id="basic-nav-dropdown">
            <MenuItem eventKey={3.1}>Action</MenuItem>
            <MenuItem eventKey={3.2}>Another action</MenuItem>
            <MenuItem eventKey={3.3}>Something else here</MenuItem>
            <MenuItem divider />
            <MenuItem eventKey={3.3}>Separated link</MenuItem>
          </NavDropdown>
        </Nav>
      </Navbar>
    );
  }
}

NavigationBar.propTypes = {
  openProjectManager: React.PropTypes.func,
  currentProject: React.PropTypes.object,
};

const mapStateToProps = (state) => {
  const { currentProject } = state;
  return {
    currentProject,
  };
};

const mapDispatchToProps = (dispatch) => ({
  openProjectManager: () => {
    dispatch(openModal(PROJECT_MANAGER_MODAL_NAME));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(NavigationBar);
