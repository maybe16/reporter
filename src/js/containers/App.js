import React, { Component } from 'react';
import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

import Reporter from './Reporter';
import * as reducers from '../reducers';

const initialState = {
  currentProject: {},
  shownProject: {},
  allProjectsInfo: {},
  activeModal: '',
};

const reducer = combineReducers(reducers);

const finalCreateStore = compose(
  applyMiddleware(thunk),
  window.devToolsExtension ? window.devToolsExtension() : func => func
)(createStore);

const store = finalCreateStore(reducer, initialState);

export default class App extends Component {
  render() {
    return (
      <div>
        <Provider store={store}>
          <Reporter />
        </Provider>
      </div>
    );
  }
}
