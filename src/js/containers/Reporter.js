import React, { Component } from 'react';
import { connect } from 'react-redux';

import ProjectManager from './ProjectManager';
import NavigationBar from './NavigationBar';


class Reporter extends Component {
  render() {
    return (
      <div>
        <NavigationBar />
        <ProjectManager />
      </div>
    );
  }
}

export default connect()(Reporter);
