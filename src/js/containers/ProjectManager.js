import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Modal, Button, Grid, Col, Row, Glyphicon, ButtonGroup } from 'react-bootstrap';

import * as actions from '../actions/Projects';
import { PROJECT_MANAGER_MODAL_NAME } from '../constants/ModalsNames';
import { closeModal } from '../actions/Modals';
import * as dataStatuses from '../constants/DataStatuses';
import * as actionsTypes from '../constants/ActionTypes';

import EditProjectForm from '../components/EditProjectForm';
import AllProjectsList from '../components/AllProjectsList';

class ProjectManagerView extends Component {

  componentWillMount() {
    this.props.getAllProjects();
  }

  render() {
    const {
      getAllProjects,
      close,
      isOpen,
      currentProject,
      allProjectsInfo,
      selectProject,
      showProject,
      updateProject,
      shownProject,
    } = this.props;

    const allProjectsLoads = allProjectsInfo.status === dataStatuses.DATA_LOADS;

    return (
      <Modal show={isOpen} onHide={close} bsSize="large"
        dialogClassName="full-height" id="projectManager"
      >
        <Modal.Header closeButton>
          <Modal.Title>Project manager</Modal.Title>
        </Modal.Header>
        <Modal.Body className="unpadding">
          <Grid fluid className="full-height">
            <Row className="divided-columns">
              <Col lg={6} md={6} sm={6} className="project-form-xs">
                <EditProjectForm
                  shownProject={shownProject}
                  updateProject={updateProject}
                />
              </Col>
              <Col lg={6} md={6} sm={6}>
                <Row className="panel-head-row">
                  <Col lg={12}>
                    <ButtonGroup className="pull-right">
                      <Button
                        onClick={getAllProjects}
                        disabled={allProjectsLoads}
                      >
                        <Glyphicon glyph="plus" />
                      </Button>
                      <Button
                        onClick={getAllProjects}
                        disabled={allProjectsLoads}
                      >
                        <Glyphicon glyph={`refresh ${allProjectsLoads ? 'glyphicon-spin' : ''}`} />
                      </Button>
                    </ButtonGroup>
                    <span className="panel-title">All projects</span>
                  </Col>
                </Row>
                <Row className="full-height scrolled" id="projectsListRow">
                  <Col lg={12}>
                    <AllProjectsList
                      currentProject={currentProject}
                      allProjectsInfo={allProjectsInfo}
                      selectProject={selectProject}
                      showProject={showProject}
                      shownProject={shownProject}
                    />
                  </Col>
                </Row>
              </Col>
            </Row>
          </Grid>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={close}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

ProjectManagerView.propTypes = {
  currentProject: React.PropTypes.object,
  shownProject: React.PropTypes.object,
  allProjectsInfo: React.PropTypes.object,
  getAllProjects: React.PropTypes.func,
  selectProject: React.PropTypes.func,
  showProject: React.PropTypes.func,
  updateProject: React.PropTypes.func,
  close: React.PropTypes.func,
  isOpen: React.PropTypes.bool,
};

const mapStateToProps = (state) => {
  const { currentProject, allProjectsInfo, activeModal, shownProject } = state;
  return {
    currentProject,
    allProjectsInfo,
    shownProject,
    isOpen: activeModal.name === PROJECT_MANAGER_MODAL_NAME,
  };
};

const mapDispatchToProps = (dispatch) => ({
    getAllProjects: () => {
      dispatch(actions.getAllProjects());
    },
    selectProject: (projectId) => {
      dispatch(actions.selectProject(projectId));
    },
    close: () => {
      dispatch(closeModal(PROJECT_MANAGER_MODAL_NAME));
    },
    updateProject: (updatedProject) => {
      dispatch(actions.updateProject(updatedProject));
    },
    showProject: (project) => {
      dispatch({
        type: actionsTypes.SHOWN_PROJECT_SET,
        project,
      });
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(ProjectManagerView);
