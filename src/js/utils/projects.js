import * as dataStatuses from '../constants/DataStatuses';

export function sameProject(proj1, proj2) {
  return (
    proj1 && proj2 &&
    proj1.id === proj2.id
  );
}

export function findProject(where, proj) {
  const found = where.find(
    (project) => sameProject(project, proj)
  );
  return found;
}

export function projectIsLoading(project) {
  return project.status === dataStatuses.DATA_LOADS;
}
