import * as types from '../constants/ActionTypes';

const initialState = {
  name: '',
};

export default function currentProject(state = initialState, action) {
  switch (action.type) {
    case types.OPEN_MODAL:
      return {
        name: action.name,
      };
    case types.CLOSE_MODAL:
      return {
        name: '',
      };
    default:
      return Object.assign({}, state);
  }
}
