import currentProjectReducer from './currentProject';
import shownProjectReducer from './shownProject';
import allProjectsInfoReducer from './allProjectsInfo';
import activeModalReducer from './activeModal';

export const currentProject = currentProjectReducer;
export const shownProject = shownProjectReducer;
export const allProjectsInfo = allProjectsInfoReducer;
export const activeModal = activeModalReducer;
