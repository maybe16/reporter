import * as types from '../constants/ActionTypes';
import * as dataStatuses from '../constants/DataStatuses';
import { deepClone } from '../utils/common';

const initialState = {};
let lastRequestedProjectId = '';

export default function currentProject(state = initialState, action) {
  let newState = deepClone(state);
  switch (action.type) {
    case types.PROJECT_GET_REQUEST:
      lastRequestedProjectId = action.id;
      return {
        id: action.id,
        status: dataStatuses.DATA_LOADS,
      };
    case types.PROJECT_GET_SUCCESS: {
      if (action.project.id === lastRequestedProjectId) {
        newState = {
          ...action.project,
          status: dataStatuses.DATA_RECIEVED,
        };
      }
      return newState;
    }
    case types.PROJECT_GET_ERROR:
      return {
        ...newState,
        status: dataStatuses.DATA_RECIEVED_WITH_ERRORS,
      };
    default:
      return newState;
  }
}
