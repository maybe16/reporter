import * as types from '../constants/ActionTypes';
import * as dataStatuses from '../constants/DataStatuses';
import { deepClone } from '../utils/common';
import { sameProject } from '../utils/projects';


const initialState = {
  projects: [],
  status: dataStatuses.DATA_NOT_RECIEVED,
};

export default function allProjectsInfo(state = initialState, action) {
  const newState = deepClone(state);
  switch (action.type) {
    case types.PROJECTS_ALL_REQUEST:
      return {
        projects: [],
        status: dataStatuses.DATA_LOADS,
      };
    case types.PROJECTS_ALL_SUCCESS:
      return {
        projects: action.projects,
        status: dataStatuses.DATA_RECIEVED,
      };
    case types.UPDATE_PROJECT: {
      const updatedProjects = newState.projects.map((project) => {
        let result = project;
        if (sameProject(project, action.updatedProject)) {
          result = action.updatedProject;
        }
        return result;
      });
      newState.projects = updatedProjects;
      return newState;
    }
    default:
      return newState;
  }
}
