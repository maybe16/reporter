import * as types from '../constants/ActionTypes';
import * as dataStatuses from '../constants/DataStatuses';
import { deepClone } from '../utils/common';

const initialState = {};

export default function shownProject(state = initialState, action) {
  const newState = deepClone(state);
  switch (action.type) {
    case types.SHOWN_PROJECT_SET:
      return {
        ...action.project,
      };
    case types.PROJECT_UPDATE_REQUEST:
      return {
        ...newState,
        status: dataStatuses.DATA_UPDATES,
      };
    case types.PROJECT_UPDATE_SUCCESS:
      return {
        ...action.updatedProject,
        status: dataStatuses.DATA_RECIEVED,
      };
    case types.PROJECT_UPDATE_ERROR:
      return {
        ...newState,
        status: dataStatuses.DATA_RECIEVED_WITH_ERRORS,
      };
    default:
      return state;
  }
}
