import React, { Component } from 'react';

export default class Spinner extends Component {
  render() {
    return (
      <span className="vcentered empty-content-label">
        {this.props.text} <span className="glyphicon glyphicon-refresh glyphicon-spin"></span>
      </span>
    );
  }
}

Spinner.propTypes = {
  text: React.PropTypes.string,
};
