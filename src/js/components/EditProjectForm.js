import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import { deepClone } from '../utils/common';
import * as dataStatuses from '../constants/DataStatuses';
import Spinner from './Spinner';
import ProjectForm from './ProjectForm';

export default class EditProjectForm extends Component {
  constructor(props) {
    super(props);

    if (props.shownProject) {
      this.state = deepClone(props.shownProject);
    }

    this.onSubmitHandler = this.handleSubmit.bind(this);
  }

  handleSubmit(updatedProject) {
    this.props.updateProject(updatedProject);
  }

  render() {
    const { shownProject } = this.props;
    const status = shownProject.status;

    let html;
    if (status === dataStatuses.DATA_LOADS) {
      html = <Spinner text="Project loads" />;
    } else if (status === dataStatuses.DATA_UPDATES) {
      html = <Spinner text="Project updates" />;
    } else if (shownProject.id) {
        html = (
          <div>
            <Row className="panel-head-row">
              <Col lg={12}>
                <span>{shownProject.origName}</span>
              </Col>
            </Row>
            <Row>
              <Col lg={12}>
                <ProjectForm
                  project={shownProject}
                  onSubmit={this.onSubmitHandler}
                />
              </Col>
            </Row>
          </div>
        );
    } else {
      html = <span className="vcentered empty-content-label">Project not selected</span>;
    }
    return html;
  }

}

EditProjectForm.propTypes = {
  shownProject: React.PropTypes.object,
  updateProject: React.PropTypes.func,
};
