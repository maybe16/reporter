import React, { Component } from 'react';
import * as dataStatuses from '../constants/DataStatuses';
import Spinner from './Spinner';
import { sameProject, projectIsLoading } from '../utils/projects';

import { Col, Row, Button, Glyphicon } from 'react-bootstrap';

export default class AllProjectsList extends Component {

  renderRow(project) {
    const {
      currentProject,
      showProject,
      shownProject,
      selectProject,
    } = this.props;

    let className = 'clickable';
    let onRowClick = null;
    let buttons = null;

    if (sameProject(project, shownProject)) {
      className = 'active';
    } else {
      onRowClick = () => {
        showProject(project);
      };
    }

    if (sameProject(project, currentProject)) {
      const isLoading = projectIsLoading(currentProject);
      buttons = (
        <Button active className="pull-right" disabled>
          <Glyphicon glyph={isLoading ? 'refresh glyphicon-spin' : 'folder-open'} />
        </Button>
      );
    } else {
      const onOpenProjectBtnClick = (evt) => {
        evt.preventDefault();
        selectProject(project.id);
      };
      buttons = (
        <Button onClick={onOpenProjectBtnClick} className="pull-right">
          <Glyphicon glyph="folder-close" />
        </Button>
      );
    }

    return (
      <Row key={project.id} className={className} onClick={onRowClick}>
        <Col lg={12} md={12} sm={12}>
          {buttons}
          <span className="project-name">{project.origName}</span>
        </Col>
      </Row>
    );
  }

  renderList() {
    const { allProjectsInfo } = this.props;
    const projectsRows = allProjectsInfo.projects.map((project) => this.renderRow(project));
    return projectsRows;
  }

  render() {
    const { allProjectsInfo } = this.props;

    let html;

    if (allProjectsInfo.status === dataStatuses.DATA_NOT_RECIEVED) {
      html = <span className="vcentered empty-content-label">Data not recieved</span>;
    } else if (allProjectsInfo.status === dataStatuses.DATA_LOADS) {
      html = <Spinner text="Recieving projects" />;
    } else if (allProjectsInfo.status === dataStatuses.DATA_RECIEVED) {
      if (allProjectsInfo.projects.length) {
        html = <div className="interactiveList">{this.renderList()}</div>;
      } else {
        html = <span className="vcentered empty-content-label">No projects</span>;
      }
    }

    return html;
  }
}

AllProjectsList.propTypes = {
  currentProject: React.PropTypes.object,
  shownProject: React.PropTypes.object,
  allProjectsInfo: React.PropTypes.object,
  selectProject: React.PropTypes.func,
  showProject: React.PropTypes.func,
};
