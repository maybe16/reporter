import React, { Component } from 'react';
import { deepClone } from '../utils/common';
import { Input, FormControls, ButtonInput } from 'react-bootstrap';


const initialState = {
  origName: '',
  id: '',
  valid: true,
};

export default class ProjectForm extends Component {
  constructor(props) {
    super(props);

    this.state = initialState;
    const { project } = props;
    if (project) {
      this.state = deepClone(project);
      this.state.valid = true;
    }

    this.origNameChangeHandler = this.handleOrigNameChange.bind(this);
    this.onSubmitHandler = this.handleSubmit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const formProjInfo = this.state;
    const propProject = nextProps.project;
    if (!(
        formProjInfo && propProject &&
        formProjInfo.id === propProject.id
      )) {
      this.setState({ ...propProject });
    } else {
      this.setState({ ...formProjInfo });
    }
  }

  isOrigNameValid(value) {
    return value !== '';
  }

  validationState() {
    const formProjInfo = this.state;
    const propProject = this.props.project;
    let result;
    if (formProjInfo.origName !== propProject.origName) {
      const valid = this.isOrigNameValid(formProjInfo.origName);

      result = valid ? 'success' : 'error';
    }
    return result;
  }

  handleOrigNameChange() {
    const value = this.refs.inputOrigName.getValue();
    const validForm = this.isOrigNameValid(value);
    this.setState({
      origName: value,
      valid: validForm,
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const updatedProject = deepClone(this.props.project);
    updatedProject.origName = this.state.origName;
    this.props.onSubmit(updatedProject);
  }

  render() {
    const formProjInfo = this.state;
    const valid = this.state.valid;
    return (
      <form onSubmit={this.onSubmitHandler} className="form-horizontal">
        <FormControls.Static
          label="Folder name"
          labelClassName="col-md-4"
          wrapperClassName="col-md-8"
          value={formProjInfo.id}
        />
        <Input
          type="text"
          value={formProjInfo.origName}
          placeholder="Enter project name"
          label="Name"
          bsStyle={this.validationState()}
          hasFeedback
          ref="inputOrigName"
          labelClassName="col-md-4"
          wrapperClassName="col-md-8"
          onChange={this.origNameChangeHandler}
        />
        <div className="text-center">
          <ButtonInput
            type="submit"
            value="Submit Button"
            disabled={!valid}
          />
        </div>
      </form>
    );
  }
}

ProjectForm.propTypes = {
  project: React.PropTypes.object,
  onSubmit: React.PropTypes.func,
};
