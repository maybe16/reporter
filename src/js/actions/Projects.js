import * as types from '../constants/ActionTypes';
import * as apiUrls from '../constants/Server';


export function getAllProjects() {
  return dispatch => {

    dispatch({
      type: types.PROJECTS_ALL_REQUEST,
    });

    fetch(apiUrls.API_PROJECTS_GET_ALL_INFO)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        return response.json();
      })
      .then((json) => {
        dispatch({
          type: types.PROJECTS_ALL_SUCCESS,
          projects: json,
        });
      }).catch((error) => {
        dispatch({
          type: types.PROJECTS_ALL_ERROR,
          errorMessage: error,
        });
      });
  };
}

export function selectProject(id) {
  return dispatch => {
    dispatch({
      type: types.PROJECT_GET_REQUEST,
      id,
    });

    fetch(apiUrls.API_PROJECTS_GET, {
      method: 'POST',
      body: JSON.stringify({
        id,
      }),
    })
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        return response.json();
      })
      .then((json) => {
        dispatch({
          type: types.PROJECT_GET_SUCCESS,
          project: json,
        });
      })
      .catch((error) => {
        dispatch({
          type: types.PROJECT_GET_ERROR,
          errorMessage: error,
        });
      });
  };
}

export function updateProject(updatedProject) {
  return dispatch => {

    dispatch({
      type: types.PROJECT_UPDATE_REQUEST,
    });

    fetch(apiUrls.API_PROJECTS_UPDATE, {
      method: 'POST',
      body: JSON.stringify(updatedProject),
    })
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        return response.text();
      })
      .then((text) => {
        if (text === 'success') {
          dispatch({
            type: types.PROJECT_UPDATE_SUCCESS,
            updatedProject,
          });

          dispatch({
            type: types.UPDATE_PROJECT,
            updatedProject,
          });
        } else {
          dispatch({
            type: types.PROJECT_UPDATE_ERROR,
            errorMessage: 'fail',
          });
        }
      })
      .catch((error) => {
        dispatch({
          type: types.PROJECT_UPDATE_ERROR,
          errorMessage: error,
        });
      });
  };
}
