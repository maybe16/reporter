import * as actions from '../constants/ActionTypes';

export function closeModal(name) {
  return {
    type: actions.CLOSE_MODAL,
    name,
  };
}

export function openModal(name) {
  return {
    type: actions.OPEN_MODAL,
    name,
  };
}
