export const API_PORT = '3000';
export const API_URL = `//${window.location.hostname}:${API_PORT}/api`;

export const API_PROJECTS = `${API_URL}/projects`;
export const API_PROJECTS_CREATE = `${API_PROJECTS}/create`;
export const API_PROJECTS_REMOVE = `${API_PROJECTS}/remove`;
export const API_PROJECTS_GET = `${API_PROJECTS}/get`;
export const API_PROJECTS_GET_ALL_INFO = `${API_PROJECTS}/getAllInfo`;
export const API_PROJECTS_UPDATE = `${API_PROJECTS}/update`;
export const API_PROJECTS_PARSE_PROJECT_SOURCE = `${API_PROJECTS}/parseProjectSource`;
