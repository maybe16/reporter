var fs = require('fs');

function FileManager() {

	return {
		getFileAsText: getFileAsText,
		getFileAsOdject: getFileAsOdject,
		updateFile: updateFile
	};

	function getFileAsText(path, encoding){
		var success = true,
			str;

		if(!path) { return false; }

		try{
			var buff = fs.readFileSync(path);
			encoding = encoding ? encoding : "utf8";
			if(encoding){
				str = iconv.decode(buff, encoding);
			} else {
				str = buff.toString("utf8");
			}
			l.log("- readed project file", path);
		} catch(e) {
			l.error("Can't read project file".red, path);
			success = false;
		}

		return success ? str : false;
	}

	function getFileAsOdject(path, encoding){
		var data = getFileAsText(path, encoding);
		return JSON.parse(data);
	}

	function updateFile(path, data){
		if(!path) { return false; }

		var success = true;
		if(typeof(data) == 'object'){
			data = JSON.stringify(data);
		}
		try{
			fs.writeFileSync(path, data);
			l.log("- updated file", path);
		} catch(e) {
			l.error("Can't read project file".red, path);
			success = false;
		}
		return success;
	}
};

module.exports = new FileManager();
