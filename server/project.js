var fs = require('fs');
    iconv = require('iconv-lite'),
    stringex = require('stringex'),
    rmdir = require('rimraf');
    l = require('./logger')(module),
    fm = require('./file_manager'),
    extend = require('extend');
	parser = require('./parser');

    var pathConfig = {
        projectsId: './projects',
        sourceSubDir: 'source',
        projInfoJson: 'info.json',
        projDataJson: 'data.json'
    };

function Project(origName) {
    this.id = this.makeAppropriateId(origName);
    this.update({
      origName: origName,
      paths: this.getPaths(),
      data: {},
    });
    if(this.doesProjectExists()){
      this.load();
    }
};

Project.prototype.create = function () {
    var success = false;

    if(!this.doesProjectExists()){
        success = this.createProjFolder();
    } else {
        l.warn('Project ' + id + ' already exists.');
    }
    return success;
};

Project.prototype.save = function () {
    var success;

    success = 	fm.updateFile(this.paths.info, this.getInfo()) &&
                fm.updateFile(this.paths.data, this.data);

    return success;
};

Project.prototype.load = function (id){
    var loadedProject = fm.getFileAsOdject(this.paths.info);
    loadedProject.data = fm.getFileAsOdject(this.paths.data);
    this.update(loadedProject);
}

Project.prototype.createProjFolder = function(){
    var info = this.getInfo();
    var success = true;

    var pathRoot = this.paths.root,
        pathSources = this.paths.sources,
        pathInfo = this.paths.info,
        pathData = this.paths.data;
    try {
        fs.mkdirSync(pathRoot);
        fs.mkdirSync(pathSources);
        fs.writeFileSync(pathInfo, JSON.stringify(info));
        fs.writeFileSync(pathData, JSON.stringify({}));
    } catch(e) {
        success = false;
    }

    return success;
}

Project.prototype.remove = function remove(){
    var success = false;
    id = this.stripSpecialChars(this.id);

    if(this.doesProjectExists(id)){
        success = this.removeProjFolder(id);
    }
    return success;
}

Project.prototype.update = function (updated){
    extend(this, updated);
}

Project.prototype.removeProjFolder = function (id){
    var success = true;
    var rootPath = this.paths.root;

    try{
        rmdir.sync(rootPath);
    } catch(e){
        success = false;
    }
    return success;
}

Project.prototype.parseSource = function (id, res){
    var sourceHtml, parsedData, success, id = this.id;
    if(!this.paths.source){
        this.paths.source = this.findSourceInProjectFolder(this.paths.sources);
    }

    if(this.paths.source){
        sourceHtml = fm.getFileAsText(this.paths.source, 'win1251');

        if(sourceHtml){
            parsedData = parser.parse(sourceHtml);
            success = fm.updateFile(this.paths.data, parsedData);
        } else {
            success = false;
        }
    } else {
        success = false;
    }

    return success;
}

Project.prototype.doesProjectExists = function () {
    var path = this.paths.root;
    return fs.existsSync(path);
};

Project.prototype.getPaths = function () {
    var projFolderPath = pathConfig.projectsId + '/' + this.id + '/';

    return {
        root: projFolderPath,
        info: projFolderPath + pathConfig.projInfoJson,
        data: projFolderPath + pathConfig.projDataJson,
        sources: projFolderPath + pathConfig.sourceSubDir,
        source: this.findSourceInProjectFolder(projFolderPath + pathConfig.sourceSubDir)
    };
};

Project.prototype.stripSpecialChars = function (str){
    if(!str) return false;
    return str.replace(/[^\w\s]/gi, '');
}

Project.prototype.makeAppropriateId = function (str){
    if(!str) return false;
    str = this.stripSpecialChars(str),
    str = stringex.toASCII(str);
    str = stringex.replaceWhitespace(str, '_');
    return str;
}

Project.prototype.findSourceInProjectFolder = function (path){
    if(!fs.existsSync(path)){
        return false;
    }
    var fileList = fs.readdirSync(path),
        sourceFileName;
    if(!fileList.length){
        return false;
    }
    for(var i=0; i<fileList.length; i++){
        if(fileList[i].match(/.*\.(html|htm)/)){
            return path + '/' + fileList[i];
        }
    }

    return false;
}

Project.prototype.getAsText = function (){
    return '{"info":'+ JSON.stringify(this.getInfo()) +', "data":'+JSON.stringify(this.data)+'}';
}

Project.prototype.getInfo = function (id){
    return {
      id: this.id,
      origName: this.origName,
    };
}

module.exports = Project;
