var u = require('./utils'),
	l = require('./logger')(module);

function Router (argument) {
	var apiConfig = {};

	function setAPIConfig(configArg){
		apiConfig = configArg;
	}

	function getAPIConfig(configArg){
		return apiConfig;
	}

	function process(req, res){
		var apiCall = u.parseCommand(req.url),
			action;

		if(apiCall){
			if( apiConfig[apiCall.module] ){
				if(apiConfig[apiCall.module][apiCall.action]){
					u.waitUntilBodyReaded(req, function(data){
						l.log('REQUEST: ', data);
						action = apiConfig[apiCall.module][apiCall.action];
						if(typeof action === 'function'){
							action(data, req, res);
						} else {
							l.log('API request:',  action);
						}
					});
				} else {
					u.noSuchAction(apiCall);
				}
			} else {
				u.noSuchModule(apiCall);
			}
		}
	}

	return {
		setAPIConfig: setAPIConfig,
		getAPIConfig: getAPIConfig,
		process: process
	}
}

module.exports = new Router();
