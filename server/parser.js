var cheerio = require('cheerio'),
	l = require('./logger')(module);

function Parser() {
	var $ = cheerio.load('<div id="container"></div>');
	var config = {
		preExpressions: [
			['[\\n\\r]', ' '],
			['.*<body[^>]*>(.*)<\\/body>.*', '$1'],
			['<\\/?(?!\\/|' + ['p','b','i','h1','h2','h3','h4'].join('|') + ')[^>]*>', ''],
			['\\&nbsp;', ' '],
			['\\s+', ' '],
			['<(b|i)><\\/\\1>', ''],
			['<(b|i)\\s[^>]*>', '<$1>'],
			['<p[^>]*>', removeAttributesExceptStyle],
			['<(b|i)\\s[^>]*>', ''],
			['<(h1|h2|h3|h4)([^>]*)>', '<p  class="$1" $2>'],
			['<\\/(h1|h2|h3|h4)>', '</p>'],
			['<\\/(b|i)>\\s*<\\1>', ''],
			['<\\/(b|i)>\\s*<\\1>', '']
		],
		postExpressions: [
			['<p[^>]*>\\s*<\\/p>', '']
		]
	}
	return {
		parse: parse
	};

	function parse(source){
		var html, tree, cont, pArr, data = [];

		var label = 'Parse time';
		//to one string
		console.time(label)
		html = regExprProcess(source, config.preExpressions);
		$('#container').html(html);

		mergeBItoParentP();
		lookupForCommonClasses();

		html = $('#container').html();
		html = regExprProcess(html, config.postExpressions);
		$('#container').html(html);

		console.timeEnd(label);
		l.log('Tags count: ', $('#container').find('*').length);
		return JSON.stringify( { data: $('#container').html() } );
	}

	function mergeBItoParentP(){
		$('p b, p i').each(function(ind, el){
			el = $(el);
			parent = el.closest('p');
			if(el.is('b')){
				parent.addClass('bold');
			} else if(el.is('i')){
				parent.addClass('italic');
			}
		});
		var html = $('#container').html();
		html = html.replace(/<\/?(b|i)>/g, '');
		$('#container').html(html);
	}

	function lookupForCommonClasses() {
		$('p').each(function(ind, el){
			el = $(el);
			var style = el.attr('style');
			if(style){
				if(style.indexOf('text-align:center') !== -1){
					el.addClass('alignCenter');
					style = style.replace('text-align:center', '');
				}
				if(style.indexOf('text-align:justify') !== -1){
					el.addClass('alignJustify');
					style = style.replace('text-align:justify', '');
				}
				el.attr('style', style);
			}

		});
	}

	function regExprProcess(source, expressions){
		var i,
			result = source,
			expr;
		for(i = 0; i < expressions.length; i++){
			expr = new RegExp(expressions[i][0], 'g');
			result = result.replace(expr, expressions[i][1]);
		}
		return result;
	}

	function removeAttributesExceptStyle(str){
		var styleVal = '', res;
		var match = str.match(/style='[^']+'/);

		if(match){
			res = '<p '+ match[0] +'>';
		} else {
			res = '<p>';
		}
		return res;
	}

}



module.exports = new Parser();
