'use strict';
const fs = require('fs');
// const	u = require('./utils');
// const	fm = require('./file_manager');
const	Project = require('./project');
const	log = require('./logger')(module);
const pathConfig = {
	projectsId: './projects',
};

class ProjectManager {

  constructor() {
    if (!fs.existsSync(pathConfig.projectsId)) {
      this.createProjectsFolder();
    }

    this.activities = {
      create: [],
      update: [],
      remove: [],
    };
  }


	/*	CREATE */

	create(origName, res) {
		const proj = new Project(origName);
    this.registerActivity('create', proj.id);
		const success = proj.save();

		if (success) {
			log.success('Project created', proj.id);
			this.makeSuccResp(res, proj);
		} else {
			this.makeErrResp(res, 'fail');
		}
    this.unregisterActivity('create', proj.id);
	}

	remove(id, res) {
		const proj = new Project(id);

    this.registerActivity('remove', proj.id);
		const success = proj.remove();

		if (success) {
			log.success('Project removed', id);
			this.makeSuccResp(res);
		} else {
			log.error('Project wasn\'t removed', id);
			this.makeErrResp(res, 'fail');
		}
    this.unregisterActivity('remove', proj.id);
	}

	getOne(id, res) {
		const proj = new Project(id);
    proj.activity = this.getProjectActivity(proj.id) || null;

    setTimeout(() => {
      log.success('Project returned', id);
      this.makeSuccResp(res, proj);
    }, 1000);
	}

	getAllInfo(res) {
		const projList = fs.readdirSync(pathConfig.projectsId);
		const projects = [];

		for (let idx = 0; idx < projList.length; idx++) {
			const proj = new Project(projList[idx]);
      proj.activity = this.getProjectActivity(proj.id) || null;
      log.log(idx, proj.id);
			projects.push(proj);
		}

		log.success('All projects info returned');
		this.makeSuccResp(res, projects);
	}

	update(updatedProject, res) {
		const proj = new Project(updatedProject.id);
		proj.update(updatedProject);
    this.registerActivity('update', proj.id);

		const success = proj.save();
    setTimeout(() => {
      if (success) {
        log.success('Project updated', proj.id);
        this.makeSuccResp(res);
      } else {
        this.makeErrResp(res, 'fail');
      }
      this.unregisterActivity('update', proj.id);
    }, 2000);
	}

	/* COMPLEX ACTIONS */

	parseProjectSource(id, res) {
		const proj = new Project(id);
		const success = proj.parseSource();

		if (success) {
			log.success('Source successfully parsed and project updated', id);
			this.makeSuccResp(res);
		} else {
			this.makeErrResp(res, 'fail');
		}
	}

	createProjectsFolder() {
		try {
			fs.mkdirSync(pathConfig.projectsId);
			log.log('- created folder', pathConfig.projectsId);
		} catch (err) {
			log.error("- can't create project folders and files", pathConfig.projectsId);
		}
	}

	makeErrResp(res, message) {
		res.end(JSON.stringify({
			error: message,
		}));
	}

	makeSuccResp(res, message) {
    let response;
		if (message) {
			if (typeof message !== 'string') {
				response = JSON.stringify(message);
			}
		} else {
			response = 'success';
		}
		res.end(response);
	}

  /* MANAGE ACTIVITIES */
  registerActivity(activityName, projectName) {
    const activitiesArr = this.activities[activityName];
    if (!activitiesArr) {
      log.error('unexpected activity: ', activityName);
      return;
    }

    if (activitiesArr.indexOf(projectName) === -1) {
      activitiesArr.push(projectName);
    }
  }

  unregisterActivity(activityName, projectName) {
    const activitiesArr = this.activities[activityName];
    if (!activitiesArr) {
      log.error('unexpected activity: ', activityName);
      return;
    }

    const idx = activitiesArr.indexOf(projectName);
    if (idx !== -1) {
      activitiesArr.splice(idx, 1);
    }
  }

  getProjectActivity(projectName) {
    for (const key in this.activities) {
      if (this.activities.hasOwnProperty(key)) {
        const activity = this.activities[key];
        if (activity && activity.length && activity.indexOf(projectName) !== -1) {
          return key;
        }
      }
    }
    return null;
  }
}


module.exports = new ProjectManager();
