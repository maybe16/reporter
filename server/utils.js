function Utils() {
	return {
		waitUntilBodyReaded: waitUntilBodyReaded,
		parseCommand: parseCommand,
		noSuchModule: noSuchModule,
		noSuchAction: noSuchAction,
		getProjPaths: getProjPaths,
		doesProjectExists: doesProjectExists,
		stripSpecialChars: stripSpecialChars,
		makeAppropriateId: makeAppropriateId
	}

	function waitUntilBodyReaded(req, cb){
		var data = '';

		req.on('data', function(chunk) {

			data += chunk.toString();
		});

		req.on('end', function() {
			if(!data) data = 'false';
			cb(JSON.parse(data));
		});
	}

	function parseCommand(url){
		var parts = url.split('/').splice(1);
		if(parts[0] === 'api' && parts[1] && parts[2]){
			return {
				module: parts[1],
				action: parts[2]
			}
		} else {
			return false;
		}
	}

	function noSuchModule(command){
		console.log('There is no such module: '+command.module);
	}

	function noSuchAction(command){
		console.log('Wrong action request '+command.module+' module: '+command.action);
	}

	function getProjPaths(projId){
		var projFolderPath = config.projectsId + '/' + projId + '/';
		return {
			root: projFolderPath,
			sources: projFolderPath + config.sourceSubDir,
			info: projFolderPath + config.projInfoJson,
			data: projFolderPath + config.projDataJson
		};
	}

	function doesProjectExists(projId){
		var paths = getProjPaths(projId);
		return fs.existsSync(paths.root);
	}

	function stripSpecialChars(str){
		return str.replace(/[^\w\s]/gi, '');
	}

	function makeAppropriateId(str){
		str = stripSpecialChars(str),
		str = stringex.toASCII(str);
		str = stringex.replaceWhitespace(str, '_');
		return str;
	}

}

module.exports = new Utils();
