var colors = require('colors');

function Logger(module) {
	var conf = {
		project_manager: {
			log: true,
			error: true,
			success: true
		}
	},

	moduleName = module.filename.replace(/^.*(\\|\/)([\w_]+)\.js$/, '$2');
	var moduleConfig = conf[moduleName];

	return {
		log: log,
		error: error,
		success: success
	}

	function log() {
		if (moduleConfig && moduleConfig.log) {
			var args = Array.prototype.slice.call(arguments);
			console.log.apply(console, args);
		}
	}
	function success() {
		if (moduleConfig && moduleConfig.success) {
			var args = Array.prototype.slice.call(arguments);
			if(args.length){
				args[0] = args[0].green;
			}
			console.log.apply(console, args);
		}
	}
	function error() {
		if (moduleConfig && moduleConfig.error) {
			var args = Array.prototype.slice.call(arguments);
			if(args.length){
				args[0] = args[0].red;
			}
			console.log.apply(console, args);
		}
	}
}

module.exports = function functionName(module) {
	return new Logger(module);
}
