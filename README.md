# Installation

1. Clone repository to local folder
1. Navigate to folder in console window
1. Install required modules globally by command **npm install supervisor webpack -g**
1. Run command **npm install**
1. To start application run command **npm start**
1. Open application in browser by address http://localhost:8080/