const path = require('path');
const webpack = require('webpack');

const API_SERVER_PORT = 3000;
const HOST = 'localhost';
const API_SERVER_URL = 'http://' + HOST + ':' + API_SERVER_PORT;

module.exports = {
	entry: [
    './src/index',
  ],
	output: {
		filename: 'boundle.js',
		path: __dirname,
    publicPath: '/public/'
	},
  devServer: {
    contentBase: "./public",
  },
	devtool: 'eval',
	watch: true,
	module: {
		loaders: [
      { test: /bootstrap-sass\\assets\\javascripts\\/, loader: 'imports?jQuery=jquery' },
      {
        test: /\.scss$/,
        loaders: ['style', 'css?sourceMap', 'sass?sourceMap'],
      },
      {
        test: /\.js$/,
        loader: 'babel',
        query: {
          presets: ['react', 'es2015', 'react-hmre'],
          plugins: ['transform-object-rest-spread'],
        },
        exclude: /node_modules/,
      },
      { test: /\.(svg|ttf|eot|woff|woff2)$/, loader: 'url?limit=100000' },
      { test: /\.(jpg|png)$/, loader: "file" }
		],
	},
  API_SERVER_PORT: API_SERVER_PORT,
  API_SERVER_URL: API_SERVER_URL,
  HOST: HOST,
};
